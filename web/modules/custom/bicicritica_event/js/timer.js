/**
 * @file
 * Bicicritica event behaviors.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Behavior description.
   */
  Drupal.behaviors.bicicriticaEventTimer = {
    attach: function (context, settings) {

      /**
       * Given a momentjs object calculate the last Thursday of the month.
       */
      var lastThursday = function (instant) {
        var lastDay = instant.endOf('month');
        if (lastDay.day() >= 4)
          var sub = lastDay.day() - 4;
        else
          var sub = lastDay.day() + 3;
        return lastDay.subtract(sub, 'days');
      }

      var nextEvent,diff,regional;
      regional = drupalSettings.path.currentLanguage;
      moment.locale(regional);

      nextEvent = lastThursday(moment()).hour(20).minute(0).second(0);
      // Turn miliseconds into seconds.
      diff = nextEvent.diff(moment(), 'seconds');
      // Is in the past.
      if (diff < 0) {
        // So go for next month.
        nextEvent = lastThursday(nextEvent.add(1, 'months'));
        // Generate the new diff.
        diff = nextEvent.diff(moment(), 'seconds');
      }
      // Add the next event date.
      $('.block-bicicritica-event')
        .append('<p>' + Drupal.t(
          'Next bicicrítica: @moment', 
          {'@moment': nextEvent.format('LL')}
        ) + '</p>');
      $('.block-bicicritica-event').append('<div id="bicicritica-timer"></div>')

      // For jquery countdown.
      if (regional === 'en') {
        regional = '';
      }
      // Change language if needed
      $.countdown.setDefaults($.countdown.regionalOptions[regional]);
      // Add the countdown.
      $('#bicicritica-timer').countdown({
        until: diff,
        format: 'dHM',
        layout: Drupal.t('Just') + ' {d<}{dn} {dl}, {d>}{hn} {hl}, {mn} {ml} ' + Drupal.t('for the next one')
      })

    }
  };

} (jQuery, Drupal));
